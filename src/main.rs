extern crate nalgebra;
extern crate rand;
extern crate piston;
use piston::window::*;
use piston::input::*;
extern crate sdl2_window;
use sdl2_window::*;
extern crate graphics;
use graphics::*;
use graphics::character::CharacterCache;
extern crate opengl_graphics;
use opengl_graphics::*;

mod physics;
use self::physics::*;
mod game;
use self::game::*;
mod math;
use self::math::*;

const OPENGL: OpenGL = OpenGL::V2_1;
const FRAMES_PER_SECOND: u64 = 60;

pub struct GameState<'a> {
    font: GlyphCache<'a>,
    ship: Ship,
    debris: Vec<Debris>,
    spawner: DebrisSpawner,
    high_score: u64,
}

impl<'a> GameState<'a> {

    fn new() -> GameState<'a> {
        GameState {
            font: GlyphCache::from_bytes(
                include_bytes!("prstartk.ttf"),
                (),
                TextureSettings::new().filter(Filter::Nearest),
            ).unwrap(),
            ship: Ship::new(),
            debris: Vec::new(),
            spawner: DebrisSpawner::new(),
            high_score: 0,
        }
    }

    fn handle_event<W>(&mut self, window: &mut W, gl: &mut GlGraphics, event: Event)
    where
        W: Window,
    {
        if let Some(args) = event.button_args() {
            match args.button {
                Button::Keyboard(Key::W) => {
                    self.ship.forward_thrust = args.state == ButtonState::Press;
                },
                Button::Keyboard(Key::S) => {
                    self.ship.reverse_thrust = args.state == ButtonState::Press;
                },
                Button::Keyboard(Key::A) => {
                    self.ship.left_thrust = args.state == ButtonState::Press;
                },
                Button::Keyboard(Key::D) => {
                    self.ship.right_thrust = args.state == ButtonState::Press;
                },
                Button::Keyboard(Key::Space) => match args.state {
                    ButtonState::Press => {
                        self.ship.grab_nearest(&self.debris);
                    },
                    ButtonState::Release => {
                        self.ship.release();
                    },
                },
                _ => {},
            }
        }

        if let Some(args) = event.render_args() {
            gl.draw(args.viewport(), |context, graphics| {
                let Size {width, height} = window.draw_size();
                // Despawn objects that are out of the display area
                let display_radius = 
                    ((width as f64 / 2.0).powi(2) + (height as f64 / 2.0).powi(2)).sqrt() + 30.0;
                let display_circle = Circle::new(Point::new(0.0, 0.0), display_radius);

                let display_box = Rect::new(
                    Point::new(0.0, 0.0),
                    width as f64 / 2.0,
                    height as f64 / 2.0,
                );

                self.debris.retain(|d| cc_collides_with(d, &display_circle));

                if let Some(spawned) = self.spawner.try_spawn(display_radius) {
                    self.debris.push(spawned);
                }

                let num_debris = self.debris.iter()
                    .filter(|&d| cb_collides_with(d, &display_box))
                    .count();
                let norm_score = (1_000_000_000.0 * num_debris as f64 / (width * height) as f64) as u64;
                if norm_score > self.high_score {
                    self.high_score = norm_score;
                }

                // Perform collisions and position calculations for objects
                for i in 0..self.debris.len() {
                    for j in i+1..self.debris.len() {
                        if cc_collides_with(&self.debris[i], &self.debris[j]) {
                            let (iv, jv) = calculate_collision(&self.debris[i], &self.debris[j]);
                            let ic = cc_contact_point(&self.debris[i], &self.debris[j]);
                            let jc = cc_contact_point(&self.debris[j], &self.debris[i]);
                            self.debris[i].position += (jc - ic) / 1.99;
                            self.debris[j].position += (ic - jc) / 1.99;
                            self.debris[i].velocity = iv;
                            self.debris[j].velocity = jv;
                        }
                    }
                    if cc_collides_with(&self.debris[i], &self.ship) {
                        let (iv, sv) = calculate_collision(&self.debris[i], &self.ship);
                        let ic = cc_contact_point(&self.debris[i], &self.ship);
                        let sc = cc_contact_point(&self.ship, &self.debris[i]);
                        self.debris[i].position += (sc - ic) / 1.99;
                        self.ship.position += (ic - sc) / 1.99;
                        self.debris[i].velocity = iv;
                        self.ship.velocity = sv;
                    }
                    self.debris[i].position += self.debris[i].velocity;
                }
                self.ship.update();

                // Respawn if out of bounds
                if !cb_collides_with(&self.ship, &display_box) {
                    self.debris.clear();
                    self.ship = Ship::new();
                    self.spawner = DebrisSpawner::new();
                }

                // Draw it!
                let context = context
                    .trans(width as f64 / 2.0, height as f64 / 2.0);

                clear([0.0, 0.0, 0.0, 1.0], graphics);
                for i in &self.debris {
                    i.draw(context, graphics);
                }
                self.ship.draw(context, graphics);

                text(
                    [1.0, 1.0, 1.0, 1.0],
                    16,
                    &format!("SCORE {}", norm_score),
                    &mut self.font,
                    context.transform
                        .trans(-(width as f64) / 2.0 + 20.0, (height as f64) / 2.0 - 20.0),
                    graphics,
                ).unwrap();
                
                let high_score_text = format!("{} HIGH", self.high_score);
                let hst_width = self.font.width(16, &high_score_text).unwrap();
                text(
                    [1.0, 1.0, 1.0, 1.0],
                    16,
                    &high_score_text,
                    &mut self.font,
                    context.transform
                        .trans(
                            (width as f64) / 2.0 - 20.0 - hst_width,
                            (height as f64) / 2.0 - 20.0,
                        ),
                    graphics,
                ).unwrap();
            });
        }
    }
}

fn main() {
    let window: Sdl2Window = WindowSettings::new(
            "AstroHoarders", [640, 480]
        )
        .opengl(OPENGL).srgb(false)
        .build().unwrap();
    let gl = GlGraphics::new(OPENGL);
    let state = GameState::new();

    #[cfg(not(target_os = "emscripten"))] {
        use piston::event_loop::*;
        let (mut window, mut gl, mut state) = (window, gl, state);
        let mut events = Events::new(
            EventSettings::new()
                .max_fps(FRAMES_PER_SECOND)
                .ups(0)
        );

        while let Some(event) = events.next(&mut window) {
            state.handle_event(&mut window, &mut gl, event);
        }
    }

    #[cfg(target_os = "emscripten")] {
        emscripten::begin_main_loop(window, gl, state);
    }
}


#[cfg(target_os = "emscripten")]
mod emscripten {
    extern crate emscripten_sys;

    use std::os::raw::c_void;
    use std::mem;

    use piston::window::Window;
    use piston::input::*;
    use opengl_graphics::GlGraphics;

    use {GameState, FRAMES_PER_SECOND};

    type GameContext<'a, W> = (W, GlGraphics, GameState<'a>);

    pub fn begin_main_loop<W>(window: W, gl: GlGraphics, state: GameState)
    where
        W: Window,
    {
        let context: Box<GameContext<W>> = Box::new((window, gl, state));
        unsafe {
            emscripten_sys::emscripten_set_main_loop_arg(
                Some(main_loop_wrapper::<W>),
                Box::into_raw(context) as *mut c_void,
                FRAMES_PER_SECOND as i32, 1
            );
        }
    }

    unsafe extern "C" fn main_loop_wrapper<W>(arg: *mut c_void)
    where
        W: Window,
    {
        let (ref mut window, ref mut gl, ref mut state): &mut GameContext<W>
            = Box::leak(Box::from_raw(mem::transmute(arg)));

        window.swap_buffers();
        state.handle_event(window, gl, Event::Loop(Loop::AfterRender(AfterRenderArgs)));

        while let Some(event) = window.poll_event() {
            state.handle_event(window, gl, Event::Input(event));
        }

        if window.should_close() {
            emscripten_sys::emscripten_cancel_main_loop();
            return;
        }


        let size = window.size();
        let draw_size = window.draw_size();
        state.handle_event(window, gl, Event::Loop(Loop::Render(RenderArgs {
            ext_dt: 1.0 / (FRAMES_PER_SECOND as f64),
            width: size.width,
            height: size.height,
            draw_width: draw_size.width,
            draw_height: draw_size.height,
        })));
    }
}
