use nalgebra::{distance_squared};

use math::{Scalar, Point, Vector};

pub trait BoundingCircle {

    fn center(&self) -> Point;

    fn radius(&self) -> Scalar;
}

pub trait BoundingBox {

    fn center(&self) -> Point;

    fn x_radius(&self) -> Scalar;

    fn y_radius(&self) -> Scalar;
}

pub trait PhysicsObject: BoundingCircle {

    fn mass(&self) -> Scalar;

    fn angular_mass(&self) -> Scalar {
        self.mass() * self.radius() * self.radius() / 2.0
    }

    fn velocity(&self) -> &Vector;
    
    fn velocity_mut(&mut self) -> &mut Vector;
}

pub fn cc_collides_with<T, U>(t: &T, u: &U) -> bool
where
    T: BoundingCircle,
    U: BoundingCircle,
{
    distance_squared(&t.center(), &u.center()) <= (t.radius() + u.radius()).powi(2)
}

pub fn cc_contact_point<T, U>(t: &T, u: &U) -> Point
where
    T: BoundingCircle,
    U: BoundingCircle,
{
    let v = u.center() - t.center();
    t.center() + (v * (t.radius() / v.norm()))
}

pub fn bc_collides_with<T, U>(t: &T, u: &U) -> bool
where
    T: BoundingBox,
    U: BoundingCircle,
{
    let distance = (t.center() - u.center()).abs();

    distance.x < t.x_radius() + u.radius()
    && distance.y < t.y_radius() + u.radius()
    && (
        distance.x <= t.x_radius() 
        || distance.y <= t.y_radius()
        || (distance - Vector::new(t.x_radius(), t.y_radius())).norm_squared() < u.radius().powi(2)
    )
}

pub fn cb_collides_with<T, U>(t: &T, u: &U) -> bool
where
    T: BoundingCircle,
    U: BoundingBox,
{
    bc_collides_with(u, t)
}

const RETAINED_ENERGY: Scalar = 0.9;
const RETAINED_VELOCITY: Scalar = RETAINED_ENERGY * RETAINED_ENERGY;

pub fn calculate_collision<T, U>(t: &T, u: &U) -> (Vector, Vector)
where
    T: PhysicsObject,
    U: PhysicsObject,
{
    let dvt = RETAINED_VELOCITY * 2.0 * u.mass() * (t.center() - u.center())
        * (t.velocity() - u.velocity()).dot(&(t.center() - u.center()))
        / ((t.mass() + u.mass()) * (t.center() - u.center()).norm_squared());

    let dvu = RETAINED_VELOCITY * 2.0 * t.mass() * (u.center() - t.center())
        * (u.velocity() - t.velocity()).dot(&(u.center() - t.center()))
        / ((u.mass() + t.mass()) * (t.center() - u.center()).norm_squared());

    (t.velocity() - dvt, u.velocity() - dvu)
}
