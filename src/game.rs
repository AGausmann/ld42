use std::f64;
use std::f64::consts::PI;
use std::time::{Instant, Duration};

use rand::{Rng, thread_rng};
use rand::distributions::Uniform;
use math::{Point, Scalar, Vector};
use physics::{BoundingCircle, BoundingBox, PhysicsObject};
use graphics::{polygon, line, ellipse, Context, Graphics, Transformed};
use graphics::types::Color;

pub const SHIP_MASS: f64 = 40.0;
pub const ARM_LENGTH: f64 = 20.0;
pub const LINEAR_THRUST: f64 = 2.5;
pub const ANGULAR_THRUST: f64 = 3.0;

pub struct Circle {
    pub center: Point,
    pub radius: Scalar,
}

impl Circle {
    pub fn new(center: Point, radius: Scalar) -> Circle {
        Circle {
            center,
            radius,
        }
    }
}

impl BoundingCircle for Circle {
    fn center(&self) -> Point {
        self.center
    }

    fn radius(&self) -> Scalar {
        self.radius
    }
}

pub struct Rect {
    pub center: Point,
    pub x_radius: Scalar,
    pub y_radius: Scalar,
}

impl Rect {
    pub fn new(center: Point, x_radius: Scalar, y_radius: Scalar) -> Rect {
        Rect {
            center,
            x_radius,
            y_radius,
        }
    }
}

impl BoundingBox for Rect {
    fn center(&self) -> Point {
        self.center
    }

    fn x_radius(&self) -> Scalar {
        self.x_radius
    }

    fn y_radius(&self) -> Scalar {
        self.y_radius
    }
}

#[derive(Debug, Clone)]
pub struct Ship {
    pub color: Color,
    pub position: Point,
    pub angle: Scalar,
    pub velocity: Vector,
    pub angular_velocity: Scalar,
    pub forward_thrust: bool,
    pub reverse_thrust: bool,
    pub left_thrust: bool,
    pub right_thrust: bool,
    pub holding: Option<usize>,
}

impl Ship {
    pub fn new() -> Ship {
        Ship {
            color: [1.0, 1.0, 1.0, 1.0],
            position: Point::new(0.0, 0.0),
            angle: 0.0,
            velocity: Vector::new(0.0, 0.0),
            angular_velocity: 0.0,
            forward_thrust: false,
            reverse_thrust: false,
            left_thrust: false,
            right_thrust: false,
            holding: None,
        }
    }

    pub fn release(&mut self) {
        self.holding = None;
    }

    pub fn grabber_center(&self) -> Point {
        self.position + Vector::new(
            ARM_LENGTH * self.angle.cos(),
            ARM_LENGTH * self.angle.sin(),
        )
    }

    pub fn grab_nearest<'a, I>(&mut self, iter: I)
    where
        I: IntoIterator<Item=&'a Debris>,
    {
        let target = self.grabber_center();

        self.holding = iter.into_iter().enumerate()
            .fold(None, |acc, (i, d)| {
                let nearest = acc.map(|(_, x)| x).unwrap_or(f64::INFINITY);
                let dist = (d.position - target).norm_squared();

                if dist < nearest {
                    Some((i, dist))
                } else {
                    acc
                }
            })
            .map(|(i, _)| i);
    }

    pub fn update(&mut self) {
        self.position += self.velocity;
        if self.forward_thrust {
            self.velocity += Vector::new(
                LINEAR_THRUST * self.angle.cos() / self.mass(),
                LINEAR_THRUST * self.angle.sin() / self.mass(),
            );
        }
        if self.reverse_thrust {
            self.velocity -= Vector::new(
                LINEAR_THRUST * self.angle.cos() / self.mass(),
                LINEAR_THRUST * self.angle.sin() / self.mass(),
            );
        }
        self.angle += self.angular_velocity;
        if self.left_thrust {
            self.angular_velocity -= ANGULAR_THRUST / self.angular_mass();
        }
        if self.right_thrust {
            self.angular_velocity += ANGULAR_THRUST / self.angular_mass();
        }
    }

    pub fn draw<G>(&self, context: Context, graphics: &mut G)
    where
        G: Graphics,
    {

        let transform = context.transform
            .trans(self.position.x, self.position.y)
            .rot_rad(self.angle);

        if self.forward_thrust {
            polygon(
                [1.0, 1.0, 1.0, 1.0],
                &[
                    [-15.0, 0.0],
                    [-17.5, 5.0],
                    [-30.0, 0.0],
                    [-17.5, -5.0],
                ],
                transform,
                graphics,
            );
        }

        if self.reverse_thrust {
            polygon(
                [1.0, 1.0, 1.0, 1.0],
                &[
                    [-5.0, 6.25],
                    [10.0, 2.5],
                    [12.5, 7.0],
                ],
                transform,
                graphics,
            );
            polygon(
                [1.0, 1.0, 1.0, 1.0],
                &[
                    [-5.0, -6.25],
                    [10.0, -2.5],
                    [12.5, -7.0],
                ],
                transform,
                graphics,
            );
        }

        line(
            self.color,
            1.0,
            [-15.0, 0.0, -20.0, 10.0],
            transform,
            graphics,
        );
        line(
            self.color,
            1.0,
            [-20.0, 10.0, 20.0, 0.0],
            transform,
            graphics,
        );
        line(
            self.color,
            1.0,
            [20.0, 0.0, -20.0, -10.0],
            transform,
            graphics,
        );
        line(
            self.color,
            1.0,
            [-20.0, -10.0, -15.0, 0.0],
            transform,
            graphics,
        );

        #[cfg(debug_assertions)]
        draw_bcircle(self, context, graphics);
    }
}

impl BoundingCircle for Ship {
    fn center(&self) -> Point {
        self.position
    }

    fn radius(&self) -> Scalar {
        15.0
    }
}

impl PhysicsObject for Ship {
    fn mass(&self) -> Scalar {
        SHIP_MASS
    }

    fn velocity(&self) -> &Vector {
        &self.velocity
    }

    fn velocity_mut(&mut self) -> &mut Vector {
        &mut self.velocity
    }
}

pub struct Debris {
    pub color: Color,
    pub size: Scalar,
    pub mass: Scalar,
    pub position: Point,
    pub angle: Scalar,
    pub velocity: Vector,
    pub angular_velocity: Scalar,
}

impl Debris {
    pub fn new(
        size: Scalar,
        mass: Scalar,
        position: Point,
        velocity: Vector,
        angular_velocity: Scalar
    ) -> Debris {
        Debris {
            color: [1.0, 1.0, 1.0, 1.0],
            size,
            mass,
            position,
            angle: 0.0,
            velocity,
            angular_velocity,
        }
    }

    pub fn random(start: Scalar) -> Debris {
        let size_dist = Uniform::new(10.0, 30.0);
        let density_dist = Uniform::new(0.2, 0.3);
        let start_angle_dist = Uniform::new(0.0, 2.0 * PI);
        let angle_offset_dist = Uniform::new(-PI / 6.0, PI / 6.0);
        let momentum_dist = Uniform::new(20.0, 80.0);

        let size: Scalar = thread_rng().sample(size_dist);
        let mass = thread_rng().sample(density_dist) * size.powi(2);
        let start_angle = thread_rng().sample(start_angle_dist);
        let velocity_angle = thread_rng().sample(angle_offset_dist) + start_angle + PI;
        let speed = thread_rng().sample(momentum_dist) / mass;

        let position = Point::new(
            start * start_angle.cos(),
            start * start_angle.sin(),
        );
        let velocity = Vector::new(
            speed * velocity_angle.cos(),
            speed * velocity_angle.sin(),
        );

        Debris::new(
            size,
            mass,
            position,
            velocity,
            0.0,
        )
    }

    pub fn draw<G>(&self, context: Context, graphics: &mut G)
    where
        G: Graphics,
    {
        let transform = context.transform
            .trans(self.position.x, self.position.y)
            .rot_rad(self.angle);

        ellipse(
            self.color,
            ellipse::centered([0.0, 0.0, self.size, self.size]),
            transform,
            graphics,
        );

        #[cfg(debug_assertions)]
        draw_bcircle(self, context, graphics);
    }
}

impl BoundingCircle for Debris {
    fn center(&self) -> Point {
        self.position
    }

    fn radius(&self) -> Scalar {
        self.size
    }
}

impl PhysicsObject for Debris {
    fn mass(&self) -> Scalar {
        self.mass
    }

    fn velocity(&self) -> &Vector {
        &self.velocity
    }

    fn velocity_mut(&mut self) -> &mut Vector {
        &mut self.velocity
    }
}

pub struct DebrisSpawner {
    next_spawn: Instant,
}

impl DebrisSpawner {
    pub fn new() -> DebrisSpawner {
        DebrisSpawner {
            next_spawn: Instant::now(),
        }
    }

    pub fn try_spawn(&mut self, start: Scalar) -> Option<Debris> {
        let now = Instant::now();
        if now > self.next_spawn {
            let millis = thread_rng().gen_range(5_000, 7_000);
            self.next_spawn += Duration::from_millis(millis);
            Some(Debris::random(start))
        } else {
            None
        }
    }
}

#[cfg(debug_assertions)]
fn draw_bcircle<T, G>(t: &T, context: Context, graphics: &mut G)
where
    T: BoundingCircle,
    G: Graphics,
{
    ellipse(
        [1.0, 0.0, 0.0, 0.5],
        ellipse::centered(
            [t.center().x, t.center().y, t.radius(), t.radius()]
        ),
        context.transform,
        graphics,
    );
}
