use nalgebra::{Vector2, Point2};

pub type Scalar = f64;
pub type Point = Point2<Scalar>;
pub type Vector = Vector2<Scalar>;
