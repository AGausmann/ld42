extern crate piston_window;
use piston_window::*;

extern crate ld42;
use ld42::physics::*;
use ld42::graphics::*;

fn main() {
    let mut window: PistonWindow =
        WindowSettings::new("Hello Piston!", [640, 480])
        .exit_on_esc(true).build().unwrap();

    let mut circle_a = Circle {
        mass: 36.0,
        velocity: Vector::new(-6.0, 0.5),
        center: Point::new(480.0, 240.0),
        radius: 60.0,
    };
    let mut circle_b = Circle {
        mass: 64.0,
        velocity: Vector::new(0.0, 0.0),
        center: Point::new(160.0, 240.0),
        radius: 80.0,
    };

    while let Some(event) = window.next() {
        window.draw_2d(&event, |context, graphics| {
            circle_a.center += circle_a.velocity;
            circle_b.center += circle_b.velocity;

            if collides_with(&circle_a, &circle_b) {
                println!("hit");
                perform_elastic_collision(&mut circle_a, &mut circle_b);
            }
            let contact_a = Circle {
                mass: 0.0,
                velocity: Vector::new(0.0, 0.0),
                center: contact_point(&circle_a, &circle_b),
                radius: 10.0,
            };
            let contact_b = Circle {
                mass: 0.0,
                velocity: Vector::new(0.0, 0.0),
                center: contact_point(&circle_b, &circle_a),
                radius: 10.0,
            };

            clear([0.05, 0.05, 0.05, 1.0], graphics);
            ellipse(
                [0.8, 0.8, 0.8, 1.0],
                circle_a.rectangle(),
                context.transform,
                graphics,
            );
            ellipse(
                [0.5, 0.5, 0.5, 1.0],
                circle_b.rectangle(),
                context.transform,
                graphics,
            );
            ellipse(
                [1.0, 0.0, 0.0, 1.0],
                contact_a.rectangle(),
                context.transform,
                graphics,
            );
            ellipse(
                [0.0, 0.0, 1.0, 1.0],
                contact_b.rectangle(),
                context.transform,
                graphics,
            );
        });
    }
}
