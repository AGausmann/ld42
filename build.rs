use std::env;

fn main() {
    if env::var("CARGO_CFG_TARGET_OS").unwrap() == "linux" {
        println!("cargo:rustc-flags=-l sndio");
    }
}
